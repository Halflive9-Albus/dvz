package dwarvesvszombies.listener;

import dwarvesvszombies.arena.Arena;
import dwarvesvszombies.arena.ArenaManager;
import dwarvesvszombies.dwarf.DragonBorn;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.UUID;

public class PlayerDeathListener implements Listener {


    private final JavaPlugin plugin;

    public PlayerDeathListener(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player player = event.getEntity().getPlayer();
        ArenaManager am = ArenaManager.getManager();
        Arena arena = am.getArena(1);
        if (arena.getPlayers().contains(player.getUniqueId())) {
            if (arena.getDwarves().contains(player.getUniqueId())) {
                arena.getDwarves().remove(player.getUniqueId());
                arena.getMonsters().add(player.getUniqueId());
                //teleport
                for (UUID uuid : arena.getPlayers()) {
                    Bukkit.getPlayer(uuid).sendMessage(ChatColor.RED + "Dwarf " + player.getDisplayName() + " has fallen! Dwarfs remaining: " + arena.getDwarves().size());
                }
            } else if (arena.getMonsters().contains(player.getUniqueId())) {
                //teleport
            }
        }
    }

}
