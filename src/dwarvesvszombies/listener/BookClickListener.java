package dwarvesvszombies.listener;

import dwarvesvszombies.arena.Arena;
import dwarvesvszombies.arena.ArenaManager;
import dwarvesvszombies.dwarf.DwarfClass;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;

public class BookClickListener implements Listener {

    private final JavaPlugin plugin;

    public BookClickListener(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerUse(PlayerInteractEvent event) {
        Player p = event.getPlayer();
        ArenaManager am = ArenaManager.getManager();
        Arena arena = am.getArena(1);
        if (arena.getPlayers().contains(p.getUniqueId())) {
            plugin.getServer().getLogger().log(Level.INFO, p.getInventory().getItemInMainHand().getType().toString());
            if (p.getInventory().getItemInMainHand().getType() == Material.BOOK && event.getAction().equals(Action.LEFT_CLICK_AIR)) {
                DwarfClass dwarfClass = arena.getClasses().get(p.getUniqueId());
                dwarfClass.executeDwarfClassAction(p);
            }
        }
    }
}

