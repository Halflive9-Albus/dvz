package dwarvesvszombies.listener;

import dwarvesvszombies.arena.Arena;
import dwarvesvszombies.arena.ArenaManager;
import dwarvesvszombies.dwarf.DragonBorn;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class WitherDeathListener implements Listener {

    private final JavaPlugin plugin;

    public WitherDeathListener(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onWitherDead(EntityDeathEvent event) {
        Bukkit.getWorld("dwarves_vs_zombies_world").getPlayers().forEach(p -> p.sendMessage(event.getEntity().getName()));
        Player player = event.getEntity().getKiller();
        ArenaManager arenaManager = ArenaManager.getManager();
        Arena arena = arenaManager.getArena(1);
        arena.getClasses().put(player.getUniqueId(), new DragonBorn());
        player.sendMessage("You are the chosen one!");
        player.getInventory().clear();
        ItemStack sword = new ItemStack(Material.DIAMOND_SWORD, 1);
        sword.addEnchantment(Enchantment.DAMAGE_ALL, 5);
        player.getInventory().addItem(sword);
        ItemStack bow = new ItemStack(Material.BOW, 1);
        bow.addEnchantment(Enchantment.ARROW_DAMAGE, 2);
        bow.addEnchantment(Enchantment.ARROW_INFINITE, 1);
        bow.addEnchantment(Enchantment.ARROW_FIRE, 1);
        player.getInventory().addItem(bow);
        player.getInventory().addItem(new ItemStack(Material.ARROW ,1));
    }

}
