package dwarvesvszombies.dwarf;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class BuilderClass implements DwarfClass {

    private final String name = "Builder";
    private final int COOLDOWN_TIME = 30;

    private Map<Material, Integer> startingItems;
    private Map<UUID, Long> buildersOnCooldown;

    public BuilderClass() {
        this.startingItems = new HashMap<>();
        this.buildersOnCooldown = new HashMap<>();
        startingItems.put(Material.BOOK, 1);
        startingItems.put(Material.IRON_PICKAXE, 1);
        startingItems.put(Material.IRON_AXE, 1);
        startingItems.put(Material.IRON_SHOVEL, 1);

    }

    public Map<Material, Integer> getStartingItems() {
        return startingItems;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void executeDwarfClassAction(Player player) {
        if (buildersOnCooldown.containsKey(player.getUniqueId())) {
            long timeLeft = System.currentTimeMillis() - buildersOnCooldown.get(player.getUniqueId());
            if(TimeUnit.MILLISECONDS.toSeconds(timeLeft) >= COOLDOWN_TIME) {
              buildersOnCooldown.remove(player.getUniqueId());
            } else {
                player.sendMessage(ChatColor.RED + "You still have to wait " + (COOLDOWN_TIME - TimeUnit.MILLISECONDS.toSeconds(timeLeft)) + " seconds before using this.");
                return;
            }
        }
        player.getInventory().addItem(new ItemStack(Material.STONE_BRICKS, 64));
        player.sendMessage(ChatColor.GREEN + "You get some building materials.");
        buildersOnCooldown.put(player.getUniqueId(), System.currentTimeMillis());
    }
}
