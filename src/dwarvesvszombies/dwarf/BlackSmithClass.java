package dwarvesvszombies.dwarf;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class BlackSmithClass implements DwarfClass {

    private final String name = "Blacksmith";
    private final int COOLDOWN_TIME = 30;

    private Map<Material, Integer> startingItems;
    private Map<UUID, Long> blackSmithsOnCooldown;
    private List<Material> blackSmithItems;

    public BlackSmithClass() {
        this.startingItems = new HashMap<>();
        this.blackSmithsOnCooldown = new HashMap<>();
        startingItems.put(Material.BOOK, 1);
        startingItems.put(Material.IRON_PICKAXE, 1);
        startingItems.put(Material.REDSTONE_ORE, 8);
        startingItems.put(Material.GOLD_ORE, 24);
        startingItems.put(Material.FURNACE, 4);
        startingItems.put(Material.COAL, 10);
        startingItems.put(Material.CHEST, 2);
        startingItems.put(Material.CRAFTING_TABLE, 1);
        startingItems.put(Material.NETHER_BRICKS, 64);
        blackSmithItems = populateBlackSmithItems();
    }

    public Map<Material, Integer> getStartingItems() {
        return startingItems;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void executeDwarfClassAction(Player player) {
        if(player.getInventory().contains(Material.CLOCK)) {
            player.getInventory().remove(new ItemStack(Material.CLOCK, 1));
            Random random = new Random();
            Material material = blackSmithItems.get(random.nextInt(blackSmithItems.size()));
            if (material.equals(Material.BOW)) {
                player.getInventory().addItem(new ItemStack(Material.ARROW, 20));
            }
            player.getInventory().addItem(new ItemStack(material, 1));
            player.getInventory().addItem(new ItemStack(Material.REDSTONE_ORE, 4));
            player.sendMessage(ChatColor.GREEN + "You turn in a clock for some weapons!");
            blackSmithsOnCooldown.put(player.getUniqueId(), System.currentTimeMillis());
        } else {
            player.sendMessage(ChatColor.RED + "You need a clock to perform this action.");
        }
    }


    private List<Material> populateBlackSmithItems() {
        List<Material> materials = new ArrayList<>();
        materials.add(Material.DIAMOND_AXE);
        materials.add(Material.DIAMOND_SWORD);
//        materials.add(Material.DIAMOND_CHESTPLATE);
//        materials.add(Material.DIAMOND_LEGGINGS);
//        materials.add(Material.DIAMOND_HELMET);
//        materials.add(Material.DIAMOND_BOOTS);
        materials.add(Material.BOW);
//        materials.add(Material.SHIELD);
        return materials;
    }

}
