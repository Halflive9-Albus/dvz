package dwarvesvszombies.dwarf;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.Map;

public interface DwarfClass {

    public String getName();

    public void executeDwarfClassAction(Player player);

    public Map<Material, Integer> getStartingItems();
}
