package dwarvesvszombies.command;

import dwarvesvszombies.lobby.LobbyManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DvzJoin implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if(args[0].equals("join")) {
                player.sendMessage("Joining dwarves vs zombies...");
                LobbyManager.getManager().addPlayer(player);
            } else if(args[0].equals("leave")) {
                player.sendMessage("Leaving dwarves vs zombies...");
                LobbyManager.getManager().removePlayer(player);
            }
        }
        // If the player (or console) uses our command correct, we can return true
        return true;
    }
}