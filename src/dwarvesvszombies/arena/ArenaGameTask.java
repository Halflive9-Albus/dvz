package dwarvesvszombies.arena;

import dwarvesvszombies.lobby.LobbyManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EnderDragon;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wither;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.UUID;

public class ArenaGameTask implements Runnable {

    private final JavaPlugin plugin;
    private boolean isRunning;
    private int counter;

    public ArenaGameTask(JavaPlugin plugin) {
        this.plugin = plugin;
        this.counter = 30;
    }
        @Override
        public void run() {
        ArenaManager am = ArenaManager.getManager();
            if (counter== 0) {
                World w =  Bukkit.getWorld("dwarves_vs_zombies_world");
                Wither wither = w.spawn(new Location(w,-1370, 125, 1096), Wither.class);
                wither.getBossBar().setVisible(true);
                am.stopWitherTask();
                return;
            }
            if (counter % 30 == 0) {
                for (UUID p : am.getArena(1).getPlayers()) {
                    Bukkit.getPlayer(p).sendMessage(ChatColor.RED +""+ (counter/30) + " minutes remaining before the dragon comes!");
                }
            }
            counter = counter - 1;
    }

    public boolean isRunning() {
        return isRunning;
    }

}
