package dwarvesvszombies.arena;

import dwarvesvszombies.dwarf.DwarfClass;
import org.bukkit.Location;

import java.util.*;

// Also NOT thread safe
public class Arena {
    // Individual arena info here

    // Ofc, this CAN'T be the ID COULD IT? (jk)
    private final int id;
    private final Location spawn;
    private final List<UUID> players = new ArrayList<UUID>();
    private final Map<UUID, DwarfClass> classes = new HashMap<UUID, DwarfClass>();
    private final List<UUID> dwarves = new ArrayList<>();
    private final List<UUID> monsters = new ArrayList<>();

    public Arena(Location spawn, int id) {
        this.spawn = spawn;
        this.id = id;
    }

    // Getters

    public int getId() {
        return this.id;
    }

    public List<UUID> getPlayers() {
        return this.players;
    }

    public Location getSpawn() {
        return spawn;
    }

    public Map<UUID, DwarfClass> getClasses() {
        return classes;
    }

    public List<UUID> getDwarves() {
        return dwarves;
    }

    public List<UUID> getMonsters() {
        return monsters;
    }
}