package dwarvesvszombies.lobby;

import dwarvesvszombies.arena.ArenaGameTask;
import dwarvesvszombies.arena.ArenaManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.UUID;

public class LobbyCountdownTask implements Runnable {

    private final JavaPlugin plugin;
    private boolean isRunning;
    private int counter;

    public LobbyCountdownTask(JavaPlugin plugin) {
        this.plugin = plugin;
        this.counter = 15;
    }
        @Override
        public void run() {
        LobbyManager lm = LobbyManager.getManager();
        ArenaManager am = ArenaManager.getManager();
            for (UUID p : lm.getLobby().getPlayers()) {
                Bukkit.getPlayer(p).setLevel(counter);
            }
            if (counter== 0) {
                if (lm.getLobby().getPlayers().size() == 0) {
                    for (UUID p : lm.getLobby().getPlayers()) {
                        Bukkit.getPlayer(p).sendMessage("Not enough players to start!");
                        lm.stopCountdown();
                        counter=60;
                    }
                    return;
                }
                for (UUID p : lm.getLobby().getPlayers()) {
                    Player player = Bukkit.getPlayer(p);
                    am.addPlayer(player, 1);
                    counter=60;
                    lm.stopCountdown();
                }
                am.setCountDownTaskId(Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new ArenaGameTask(plugin), 0L,20L));
                return;
            }
            if (counter % 5 == 0) {
                for (UUID p : lm.getLobby().getPlayers()) {
                    Bukkit.getPlayer(p).sendMessage(ChatColor.RED + "Timer remaining " + counter + " seconds");
                }
            }
            counter = counter - 1;
    }


    public boolean isRunning() {
        return isRunning;
    }


}
