package dwarvesvszombies.lobby;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

// NOT THREAD SAFE!
public class LobbyManager {
    // Singleton instance
    private static LobbyManager lm;
    int time = 0;

    // Player data
    private final Map<UUID, ItemStack[]> inv = new HashMap<UUID, ItemStack[]>();
    private final Map<UUID, ItemStack[]> armor = new HashMap<UUID, ItemStack[]>();

    // Hmm, what do you think THIS might be?
    private final List<Lobby> lobbies = new ArrayList<>();
    // Keep track of the current arena ID
    private int lobbySize = 0;
    private int countDownTaskId;
    private LobbyManager() {
    } // Prevent instantiation

    // Singleton accessor with lazy initialization
    public static LobbyManager getManager() {
        if (lm == null)
            lm = new LobbyManager();
        return lm;
    }

    /**
     * Acquires an arena based on its ID number
     *
     * @return the arena possessing the specified ID
     */
    public Lobby getLobby() {
        for (Lobby l : this.lobbies) {
            if (l.getPlayers().size() < l.getCapacity()) {
                return l;
            }
        }
        return null; // Not found
    }

    /**
     * Adds the player to an arena
     *
     * <p>Gets the arena by ID, checks that it exists,
     * and check the player isn't already in a game.</p>
     *
     * @param p the player to add
     */
    public void addPlayer(Player p) {
        Lobby l = this.getLobby();
        if (l == null) {
            p.sendMessage("Invalid lobby!");
            return;
        }

        if (this.isInLobby(p)) {
            p.sendMessage("Cannot join more than 1 game!");
            return;
        }

        if (l.getPlayers().size() >= l.getCapacity()) {
            p.sendMessage("This lobby is full.");
            return;
        }

        // Adds the player to the arena player list
        l.getPlayers().add(p.getUniqueId());
        for (UUID uuid : l.getPlayers()) {
            Player player = Bukkit.getPlayer(uuid);
            if (player != null) {
                player.sendMessage(p.getDisplayName() + " has joined Lobby" + l.getId());
            }
        }
        // Save the inventory and armor
        inv.put(p.getUniqueId(), p.getInventory().getContents());
        armor.put(p.getUniqueId(), p.getInventory().getArmorContents());

        // Clear inventory and armor
        p.getInventory().setArmorContents(null);
        p.getInventory().clear();

        // Save the players's last location before joining,
        // then teleporting them to the arena spawn
        p.teleport(l.getSpawn());
        if (l.getPlayers().size() > 0) {
            if (!l.isCountdownRunning())
                startCountdown();
            l.setCountdownRunning(true);
        }
    }

    private void startCountdown() {
        JavaPlugin plugin = (JavaPlugin) Bukkit.getServer().getPluginManager().getPlugin("DwarvesVsZombies");
        if(plugin != null)  {
            countDownTaskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new LobbyCountdownTask(plugin), 0L,20L);
        }

    }

    public void stopCountdown() {
        Bukkit.getScheduler().cancelTask(countDownTaskId);
    }

    /**
     * Removes the player from their current arena.
     *
     * <p>The player is allowed to not be in game, a check
     * will be performed to ensure the validity of the arena</p>
     *
     * @param p the player to remove from the arena
     */
    public void removePlayer(Player p) {
        Lobby l = null;

        // Searches each arena for the player
        for (Lobby lobby : lobbies) {
            if (lobby.getPlayers().contains(p.getUniqueId()))
                l = lobby;
        }

        // Check arena validity
        if (l == null) {
            p.sendMessage("Invalid operation!");
            return;
        }

        // Remove from arena player lost
        l.getPlayers().remove(p.getUniqueId());

        // Remove inventory acquired during the game
        p.getInventory().clear();
        p.getInventory().setArmorContents(null);

        // Restore inventory and armor
        p.getInventory().setContents(inv.get(p.getUniqueId()));
        p.getInventory().setArmorContents(armor.get(p.getUniqueId()));

        // Remove player data entries
        inv.remove(p.getUniqueId());
        armor.remove(p.getUniqueId());

        p.teleport(new Location(Bukkit.getWorld("world"), -1272, 66, 1609 ));

        // Heh, you're safe now :)
        p.setFireTicks(0);
    }

    /**
     * Creates an arena at the specified location
     *
     * @return the arena created
     */
    public Lobby createLobby(Location loc) {
        this.lobbySize++;

        Lobby l = new Lobby(loc, this.lobbySize);
        this.lobbies.add(l);

        return l;
    }

    /**
     * Checks if the player is currently in an arena
     *
     * @param p the player to check
     * @return {@code true} if the player is in game
     */
    public boolean isInLobby(Player p) {
        for (Lobby l : this.lobbies) {
            if (l.getPlayers().contains(p.getUniqueId()))
                return true;
        }
        return false;
    }

    // UTILITY METHODS

    public String serializeLoc(Location l) {
        return l.getWorld().getName() + "," + l.getBlockX() + "," + l.getBlockY() + "," + l.getBlockZ();
    }

    public Location deserializeLoc(String s) {
        String[] st = s.split(",");
        return new Location(Bukkit.getWorld(st[0]), Integer.parseInt(st[1]), Integer.parseInt(st[2]), Integer.parseInt(st[3]));
    }
}