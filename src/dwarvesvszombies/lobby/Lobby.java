package dwarvesvszombies.lobby;

import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

// Also NOT thread safe
public class Lobby {
    // Individual arena info here

    // Ofc, this CAN'T be the ID COULD IT? (jk)
    private final int id;
    private final Location spawn;
    private final List<UUID> players = new ArrayList<UUID>();
    private final int capacity = 100;
    private boolean countdownRunning;

    public Lobby(Location spawn, int id) {
        this.spawn = spawn;
        this.id = id;
    }

    // Getters

    public int getId() {
        return this.id;
    }

    public List<UUID> getPlayers() {
        return this.players;
    }

    public Location getSpawn() {
        return spawn;
    }

    public int getCapacity() {
        return capacity;
    }

    public boolean isCountdownRunning() {
        return countdownRunning;
    }

    public void setCountdownRunning(boolean countdownRunning) {
        this.countdownRunning = countdownRunning;
    }
}