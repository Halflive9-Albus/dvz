package dwarvesvszombies;

import dwarvesvszombies.arena.ArenaManager;
import dwarvesvszombies.command.DvzJoin;
import dwarvesvszombies.listener.BookClickListener;
import dwarvesvszombies.listener.PlayerDeathListener;
import dwarvesvszombies.listener.WitherDeathListener;
import dwarvesvszombies.lobby.LobbyManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.WorldCreator;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
    @Override
    public void onEnable() {
        super.onEnable();
        Bukkit.getPluginManager().registerEvents(new BookClickListener(this), this);
        Bukkit.getPluginManager().registerEvents(new WitherDeathListener(this), this);
        Bukkit.getPluginManager().registerEvents(new PlayerDeathListener(this), this);
        Bukkit.getServer().createWorld(new WorldCreator("dwarves_vs_zombies_world"));
        this.getCommand("dvz").setExecutor(new DvzJoin());
// Create
        ArenaManager.getManager().createArena(new Location(Bukkit.getWorld("dwarves_vs_zombies_world"), -1369, 88, 1097));
        LobbyManager.getManager().createLobby(new Location(Bukkit.getWorld("world"), -1273, 66, 1609));

    }
}
